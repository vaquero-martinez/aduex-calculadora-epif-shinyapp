# ADUEx Calculadora EPIF ShinyApp

Calculadora Salarial del EPIF realizada para la Asociación de Doctorandos de la Universidad de Extremadura ([ADUEx](https://doctorandos.unex.es)). Calcula el salario que te tienen que aportar cada mes y hasta la nómina de febrero 2020, desde la aplicación del EPIF.

La calculadora está hecha con la funcionalidad Shiny del lenguaje R. Está alojada en el servidor de RStudio shinyapps.io con una cuenta gratuita y disponible en la [web de ADUEx](https://doctorandos.unex.es/calculadora-de-la-subida-salarial-del-epif/).

Espero que sea de utilidad tanto para los doctorandos de la universidad como para los de otras universidades (puede adaptarse a otras universidades con poco esfuerzo).

Es un trabajo que ha quedado un poco en fase "beta" y puede contener algunos errores, pero creemos que en general funciona bien. Se agradece que se reporte cualquier error que se detecte.

La licencia es GNU GPL v3.
